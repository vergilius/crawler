#!/usr/bin/python
import requests
import networkx
import time
MY_ID = 10638538
API_VERSION = 5.37
API_URL = 'https://api.vk.com/method/'
TIMEOUT = 1
FIELDS = 'sex,bdate,city,country,online,online_mobile,domain,has_mobile,contacts,connections,site,education,' \
         'universities,schools,can_post,can_see_all_posts,can_see_audio,can_write_private_message,status,last_seen,' \
         'relation,relatives,counters,screen_name,maiden_name,timezone,occupation,activities,interests,music,movies,' \
         'tv,books,games,about,quotes,personal,friend_status,military,career'

toDoList = {}
doneList = {}
user = {}


def ids_to_string(ids):
    s = ''
    for uid in ids:
        s = s + str(uid) + ','
    return s


def query(url):
    json_response = {}
    try:
        json_response = requests.post(url, timeout=TIMEOUT).json()
        if json_response.get('error'):
            print(json_response.get('error'))
            return {'response': 'deactivated'}
        return json_response
    except requests.exceptions.Timeout:
        print('Timeout')
        return {'response': 'deactivated'}
    except Exception:
        print('Unexpected error')
        return {'response': 'deactivated'}


def get_friends(uid):
    URL = '{}friends.get?user_id={}&v={}'.format(API_URL, uid, API_VERSION)
    return query(URL)['response']


def get_user_data(uid, fields):
    URL = '{}users.get?user_ids={}&fields={}&v={}'.format(API_URL, uid, fields, API_VERSION)
    return query(URL)['response']


def get_users_data(ids, fields=FIELDS):
    str_ids = ids_to_string(ids)
    URL = '{}users.get?user_ids={}&fields={}&v={}'.format(API_URL, str_ids, fields, API_VERSION)

    return query(URL)['response']


def get_group_members(group_id, offset=0):
    URL = '{}groups.getMembers?group_id={}&offset={}&sort=id_asc&v={}'.format(API_URL, group_id, offset, API_VERSION)
    return query(URL)['response']


def get_all_group_members(group_id):
    max_user_in_response = 1000
    members = []
    response = get_group_members(group_id)
    members_number = response['count']
    members += response['items']
    for offset in range(1000, members_number, max_user_in_response):
        members += get_group_members(group_id, offset)['items']
    return members


def addUsers(ids):
    length = len(ids)
    counter = 0
    for uid in ids:
        counter += 1
        log = '{}/{} Processing id: {}'.format(counter, length, uid)
        print(log)
        if uid not in user:
            user[uid] = {}
        user[uid]['friends'] = get_friends(uid)


def add_users_data(ids, fields=FIELDS):
    length = len(ids)
    counter = 0
    for uid in ids:
        counter += 1
        log = '{}/{} Processing id: {}'.format(counter, length, uid)
        print(log)
        data = get_user_data(uid, fields)
        if uid not in user:
            user[uid] = {}
        for value in data:
            for key in value:
                user[uid][key] = value[key]


def unwrap(obj, key_str=''):
    fields_separator = ':'
    attr = {}
    for key in obj:
        if key_str != '':
            new_key = str(key_str) + fields_separator + str(key)
        else:
            new_key = key
        if type(obj[key]) == dict:
            new_obj = unwrap(obj[key], new_key)
            for i in new_obj:
                attr[i] = new_obj[i]
        else:
            attr[new_key] = obj[key]
    return attr


def create_graph():
    graph = networkx.Graph(directed=False)
    for i in user:
        graph.add_node(i)
        #attr = unwrap(user)
        #for key in attr:
        #    graph.node[i][key] = attr[key]
        try:
            for j in user[i]['friends']['items']:
                if i != j and i in user and j in user:
                    graph.add_edge(i, j)
        except KeyError:
            print(KeyError)
        except TypeError:
            print(TypeError)
    return graph


def main():
    tp_members = get_all_group_members('tpmailru')
    #get_users_data(tp_members[0:100])
    usrs = tp_members[0:20]
    addUsers(usrs)
    add_users_data(usrs)
    #print(user)
    g = create_graph()
    file_name = '{}.graphml'.format(time.strftime('%c'))
    networkx.write_graphml(g, file_name)
    print('hello')
    pass


if __name__ == '__main__':
    main()
